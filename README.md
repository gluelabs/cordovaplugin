# Glue Parrot

Glue è un pappagallo dispettoso!

## USO

    
Installa il plugin
    $ cordova plugin add PATH/AL/TUO/PLUGIN
    

Modifica `www/js/index.js` ed istanzia il plugin nel `onDeviceReady` o in metodo compatibile

```js
    
    parrot = window.parrot;

```
##Metodi

```js
    var success = function(message) {
        alert(message);
    }

    var failure = function() {
        alert("Error calling Hello Plugin");
    }

    parrot.saluta("IL TUO NOME", success, failure);

    parrot.gimmeTime(success, failure);

    parrot.faiDispetto(success, failure);

```
