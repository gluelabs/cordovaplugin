#import <Cordova/CDV.h>

@interface Parrot : CDVPlugin

- (void) saluta:(CDVInvokedUrlCommand*)command;
- (void) gimmeTime:(CDVInvokedUrlCommand*)command;

@end