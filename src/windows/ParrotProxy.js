// in file echopluginProxy.js
cordova.commandProxy.add("Parrot",{
    saluta: function (successCallback, errorCallback, nome ) {
        if(nome){
            successCallback("Ciao "+nome);
        }else{
            errorCallback();
        }
    },
    gimmeTime: function (successCallback, errorCallback) {
        successCallback("Sono le  "+Date.now() / 1000);
    }
});

echo:function(successCallback,errorCallback,strInput) {
        if(!strInput || !strInput.length) {
            errorCallback("Error, something was wrong with the input string. =>" + strInput);
        }
        else {
            successCallback(strInput + "echo");
        }