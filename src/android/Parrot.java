package com.gluelabs.plugin;


import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;

public class Parrot extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {

        if (action.equals("saluta")) {

            String name = data.getString(0);
            String message = "Ciao, " + name;
            callbackContext.success(message);
            return true;

        } else  if (action.equals("gimmeTime")) {

            Long tsLong = System.currentTimeMillis()/1000;
            String ts = tsLong.toString();
            String message = "Sono le " + ts;
            callbackContext.success(message);
            return true;

        } else {
            
            callbackContext.success("Hai chiesto un azione che nn conosco: "+action);
            return false;

        }
    }
}
