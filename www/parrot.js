/*global cordova, module*/

module.exports = {
    saluta: function (nome, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "Parrot", "saluta", [nome]);
    },
    gimmeTime: function (successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "Parrot", "gimmeTime", []);
    },
    faiDispetto: function (successCallback, errorCallback) {
        var _successCallback = function (message) {
            alert("Sono Dispettoso, ti faccio aspettare 3 secondi, ora aspetta!!");
            setTimeout(function () {
                successCallback("Hai aspettato abbastanza..ecco qui quello che volevi sapere: "+message);
            }, 3000);

        }
        var _errorCallback = function () {
            errorCallback();
        }

        cordova.exec(_successCallback, _errorCallback, "Parrot", "gimmeTime", []);
    }
};